package com.google.android.gms.location.sample.activityrecognition;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;


public class RotationVectorActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;

    private final float[] mAccelMatrix = new float[3];

    private Sensor mLinearAccSensor;

    private MyAccelView mAccelView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get an instance of the SensorManager
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);

        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        sensors = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);

        Log.d("SensorActivity", mSensorManager.getSensorList(Sensor.TYPE_ALL).toString());

        // find the rotation-vector sensor
        mLinearAccSensor = mSensorManager.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER);

        // initialize the rotation matrix to identity
        mAccelMatrix[ 0] = 0;
        mAccelMatrix[ 1] = 0;
        mAccelMatrix[ 2] = 0;

        setContentView(R.layout.activity_rotation_vector);

        mAccelView = (MyAccelView)findViewById(R.id.sensor_accel_view);

    }

    @Override
    protected void onResume() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onResume();
        /*mRenderer.start();
        * mGLSurfaceView.onResume();*/
        mSensorManager.registerListener(this, mLinearAccSensor, 1000);

    }

    @Override
    protected void onPause() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onPause();
        /*mRenderer.stop();
        mGLSurfaceView.onPause();*/

        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){   // Sensor.TYPE_LINEAR_ACCELERATION) {
            // convert the rotation-vector to a 4x4 matrix. the matrix
            // is interpreted by Open GL as the inverse of the
            // rotation-vector, which is what we want.
            mAccelMatrix[0] = event.values[0];
            mAccelMatrix[1] = event.values[1];
            mAccelMatrix[2] = event.values[2];

            mAccelView.setAccelMatrix(mAccelMatrix);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }





}
