package com.google.android.gms.location.sample.activityrecognition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by ahslaughter on 3/20/17.
 */

public class MyAccelView extends View {

    private int mTime = 0;

    private float[] mAccelMatrix = new float[3];

    private Bitmap myBitmap;
    private Canvas myCanvas;

    private Paint redPaint = new Paint();
    private Paint bluePaint = new Paint();
    private Paint greenPaint= new Paint();

    private Paint bgPaint = new Paint();
    private Paint labelPaint = new Paint();

    private float STROKE_WIDTH = 4.0f;


    private int X_MED = 150;
    private int Y_MED = 250;
    private int Z_MED = 350;

    private int SCALE = 30;



    public MyAccelView(Context context) {
        super(context);
        createBuffer();

    }

    public MyAccelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setupPaint();

        // Allows us to create the image the size of the parent/containing layout.
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    createBuffer();
                }
            });
        }

        createBuffer();

    }


    private void setupPaint(){
        redPaint.setColor(Color.RED);
        redPaint.setStrokeWidth(STROKE_WIDTH);
        redPaint.setTextSize(28.0f);
        redPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        bluePaint.setColor(Color.BLUE);
        bluePaint.setStrokeWidth(STROKE_WIDTH);
        bluePaint.setTextSize(28.0f);
        bluePaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        greenPaint.setColor(Color.GREEN);
        greenPaint.setStrokeWidth(STROKE_WIDTH);
        greenPaint.setTextSize(28.0f);
        greenPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        bgPaint.setColor(Color.argb(255, 50, 50, 50));

        labelPaint.setColor(Color.WHITE);
        labelPaint.setStrokeWidth(3.0f);

    }


    public void setAccelMatrix(float[] newVals) {

        mTime++;

        if (mTime > getWidth()){
            mTime = 0;
            createBuffer();
        }
        updateValues(myCanvas, mAccelMatrix.clone(), newVals.clone(), mTime);

        mAccelMatrix = newVals.clone();
        invalidate();
    }

    protected void createBuffer(){
        int w = getWidth();
        int h = getHeight();
//        if (w == null) w = 0;

        myBitmap = Bitmap.createBitmap(((w==0) ? 1 : w),
                ((h==0) ? 1 : h), Bitmap.Config.ARGB_8888);
        myCanvas = new Canvas(myBitmap);
        myCanvas.drawRect(0, 0, w, h, bgPaint);

/*        myCanvas.drawLine(0, X_MED, w, X_MED, labelPaint);
        myCanvas.drawLine(0, Y_MED, w, Y_MED, labelPaint);
        myCanvas.drawLine(0, Z_MED, w, Z_MED, labelPaint);*/

        myCanvas.drawText("X", 10, h, redPaint);
        myCanvas.drawText("Y", 35, h, bluePaint);
        myCanvas.drawText("Z", 50, h, greenPaint);

    }

    protected void updateValues(Canvas canvas, float[] oldVals, float[] newVals, int time){

        if (canvas != null) {

            canvas.drawLine(time-1, X_MED + (oldVals[0] * SCALE), time, X_MED + (newVals[0] * SCALE) , redPaint);
            canvas.drawLine(time-1, Y_MED + (oldVals[1] * SCALE), time, Y_MED + (newVals[1] * SCALE) , bluePaint);
            canvas.drawLine(time-1, Z_MED + (oldVals[2] * SCALE), time, Z_MED + (newVals[2] * SCALE) , greenPaint);

        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap(myBitmap, 0, 0,null);
    }

}
