package com.google.android.gms.location.sample.activityrecognition;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class RawSensorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_raw_sensor);
    }
}
